﻿using Business;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using System;
using System.Web.Mvc;
using Utilities;

namespace Web
{
    public class WebDependencyInstaller : IWindsorInstaller
    {
        /// <summary>
        /// Installs required components to given container.
        /// </summary>
        /// <param name="container"Input container.></param>
        /// <param name="store">Configuration store.</param>
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            ParamGuard.VerifyArgument(container, "IWindsorContainer cannot be empty or null");
            ParamGuard.VerifyArgument(store, "IConfigurationStore cannot be empty or null");

            container.Kernel.Resolver.AddSubResolver(new ListResolver(container.Kernel));

            container.Register(
                Classes.FromAssemblyNamed("Web").BasedOn<IController>().LifestyleTransient(),
                Component.For<IFizzBuzzGenerator>().ImplementedBy<FizzBuzzGenerator>().LifestyleTransient(),
                Component.For<IFizzBuzzDescriptionListConfigurator>().ImplementedBy<FizzBuzzDescriptionListConfigurator>().LifestyleTransient(),
                RegisterDivisibilityCheckers);
        }

        /// <summary>
        /// Registers all files that ends with name 'checker'.
        /// </summary>
        private static BasedOnDescriptor RegisterDivisibilityCheckers
        {
            get
            {
                return Classes.FromAssemblyNamed("Business")
                    .Where(type => type.Name.EndsWith("Checker", StringComparison.Ordinal))
                    .WithServiceAllInterfaces().LifestyleTransient();
            }
        }
    }
}