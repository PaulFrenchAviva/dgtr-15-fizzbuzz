﻿using Business;
using DTO;
using PagedList;
using System.Web.Mvc;
using Utilities;
using Web.Models;

namespace Web.Controllers
{
    /// <summary>
    /// Fizz buzz controller for generating fizz buzz list.
    /// </summary>
    public class FizzBuzzController : Controller
    {
        #region Private members

        /// <summary>
        /// Fizz buzz generator.
        /// </summary>
        private readonly IFizzBuzzGenerator fizzBuzzGenerator;
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpClient">Http client for calling rest service.</param>
        public FizzBuzzController(IFizzBuzzGenerator fizzBuzzGenerator)
        {
            this.fizzBuzzGenerator = fizzBuzzGenerator;
        }
        #endregion

        /// <summary>
        /// Renders fizz buzz index view.
        /// </summary>
        /// <returns> Returns view.</returns>
        public ActionResult Index()
        {
            var fizzBuzzInputModel = new FizzBuzzModel();
            return View(fizzBuzzInputModel);
        }

        /// <summary>
        /// Renders view for showing fizz buzz list.
        /// </summary>
        /// <param name="input">Input model details.</param>
        /// <returns>Returns view.</returns>
        //[HttpGet]
        public ActionResult GetFizzBuzzList(FizzBuzzModel input, int page = 1, int pageSize = 20)
        {
            ParamGuard.VerifyArgument(input, "FizzBuzzInputModel cannot be empty or null.");

            var fizzBuzzList = this.fizzBuzzGenerator.Generate(new FizzBuzzRequest { Number = input.Number }).FizzBuzzList;
            PagedList<FizzBuzzDto> pagedList = new PagedList<FizzBuzzDto>(fizzBuzzList, page, pageSize);
            return View("Index", new FizzBuzzModel { Number = input.Number, FizzBuzzList = pagedList });
        }
    }
}
