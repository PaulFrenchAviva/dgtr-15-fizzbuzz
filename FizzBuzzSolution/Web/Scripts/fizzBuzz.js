﻿$("#button12").click(function () {
    $("#fizzBuzzList").empty();
    $("#fizzBuzzList").append('<img class="loader" src="/Content/ajax-loader.gif" width="30" height="30" />');
    $.ajax({
        type: "GET",
        dataType: "text",
        url: "/FizzBuzz/GetFizzBuzzList",
        data: { Number: $("#Number").val() },
        success: function (data) {
            $("#fizzBuzzList").empty();
            $('#fizzBuzzList').html(data);
            var rowCount = $('#fizzbuzzlisttable tr').length;
            console.log(rowCount);
            //if (rowCount > 20) {
            //    $('#fizzbuzzlisttable').DataTable({
            //        "lengthMenu": [20, 30, 40, 50],                   
            //        "order": [],
            //        "columnDefs": [{
            //            "targets": 'no-sort',
            //            "orderable": false,
            //        }]
            //    });
            //}
            //$("fizzbuzzlisttable_length").trigger('change');
        },
        error: function (error) {
            $('#fizzBuzzList').html("<span>Something went wroing. Please try again.");
        }
    });
    return false;
});