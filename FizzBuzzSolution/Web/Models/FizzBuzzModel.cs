﻿using DTO;
using PagedList;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Web.Models
{
    /// <summary>
    /// Fizz buzz input model.
    /// </summary>
    public class FizzBuzzModel
    {
        /// <summary>
        /// Gets or sets number.
        /// </summary>
        [DisplayName("Enter a number.")]
        [Required(ErrorMessage = "Please enter a number.")]
        [RegularExpression("[+]?([0-9]+(?:[\\.][0-9]*)?|\\.[0-9]+)", ErrorMessage = "Enter only positive numbers")]
        [Range(1, 1000, ErrorMessage = "Enter a number between 1 and 1000.")]
        public int Number { get; set; }

        /// <summary>
        /// Gets or sets fizz buzz list.
        /// </summary>
        public PagedList<FizzBuzzDto> FizzBuzzList { get; set; }
    }
}