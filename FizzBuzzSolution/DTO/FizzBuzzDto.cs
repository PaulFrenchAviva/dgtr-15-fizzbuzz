﻿namespace DTO
{
    public class FizzBuzzDto
    {
        /// <summary>
        /// Gets or sets description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets text colour of fizz or buzz.
        /// </summary>
        public string TextColour { get; set; }
    }
}
