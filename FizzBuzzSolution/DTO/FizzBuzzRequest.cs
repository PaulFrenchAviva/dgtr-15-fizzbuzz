﻿using System.ComponentModel.DataAnnotations;

namespace DTO
{
    /// <summary>
    /// Fizz buzz request.
    /// </summary>
    public class FizzBuzzRequest
    {
        /// <summary>
        /// Gets or sets number.
        /// </summary>
        [Required(ErrorMessage = "Please enter a number.")]
        [RegularExpression("[+]?([0-9]+(?:[\\.][0-9]*)?|\\.[0-9]+)", ErrorMessage = "Enter only positive numbers")]
        [Range(1, 1000, ErrorMessage = "Enter a number between 1 and 1000.")]
        public int Number { get; set; }
    }
}
