﻿using System.Collections.Generic;

namespace DTO
{
    /// <summary>
    /// Fizz buzz api response.
    /// </summary>
    public class FizzBuzzResponse
    {
        /// <summary>
        /// Gets or sets error message.
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Gets or sets fizz buzz list.
        /// </summary>
        public IList<FizzBuzzDto> FizzBuzzList { get; set; }
    }
}
