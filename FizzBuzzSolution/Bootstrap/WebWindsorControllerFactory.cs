﻿using Castle.MicroKernel;
using System;
using System.Web.Mvc;

namespace Bootstrap
{
    /// <summary>
    /// Web controller factory.
    /// </summary>
    public class WebWindsorControllerFactory : DefaultControllerFactory
    {
        private readonly IKernel kernel;

        public WebWindsorControllerFactory(IKernel kernel)
        {
            this.kernel = kernel;
        }

        protected override IController GetControllerInstance(System.Web.Routing.RequestContext requestContext, Type controllerType)
        {
            if (controllerType == null)
            {
                throw new Exception("No controller found");                
            }

            return (IController)this.kernel.Resolve(controllerType);
        }

        public override void ReleaseController(IController controller)
        {
            this.kernel.ReleaseComponent(controller);
        }
    }
}
