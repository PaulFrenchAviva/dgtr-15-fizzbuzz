﻿using DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    /// <summary>
    /// Class to generate fizz buzz list given a number.
    /// </summary>
    public class FizzBuzzGenerator : IFizzBuzzGenerator
    {
        #region Provate members
        /// <summary>
        /// Divisibility checkers.
        /// </summary>
        private readonly IList<IDivisibilityChecker> divisibilityCheckers;

        /// <summary>
        /// fizz buzz description list confifuration.
        /// </summary>
        private readonly IFizzBuzzDescriptionListConfigurator fizzBuzzDescriptionListConfigurator;
        #endregion

        #region Constructor
        public FizzBuzzGenerator(
            IList<IDivisibilityChecker> divisibilityCheckers,
            IFizzBuzzDescriptionListConfigurator fizzBuzzDescriptionListConfigurator
            )
        {
            this.divisibilityCheckers = divisibilityCheckers;
            this.fizzBuzzDescriptionListConfigurator = fizzBuzzDescriptionListConfigurator;
        }
        #endregion

        /// <summary>
        /// Generates fizz buzz list.
        /// </summary>
        /// <param name="number">Input number.</param>
        /// <returns>Returns generated list.</returns>
        public FizzBuzzResponse Generate(FizzBuzzRequest input)
        {
            var validationContext = new ValidationContext(input, serviceProvider: null, items: null);
            var validationResults = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(input, validationContext, validationResults, true);

            if (!isValid)
            {
                return new FizzBuzzResponse
                {
                    ErrorMessage = validationResults.FirstOrDefault().ErrorMessage,
                    FizzBuzzList = new List<FizzBuzzDto>()
                };
            }

            IList<FizzBuzzDto> finalFizzBuzzOutputList = new List<FizzBuzzDto>();

            string[] fizzBuzzDescriptionList = this.fizzBuzzDescriptionListConfigurator.GetFizzBuzzDescriptionList(DateTime.Today.DayOfWeek);

            for (int sequence = 1; sequence <= input.Number; sequence++)
            {
                var divisibilityChecker = this.divisibilityCheckers.OrderBy(p => p.Sequence).ToList().FirstOrDefault(p => p.IsDivisible(sequence));
                finalFizzBuzzOutputList
                    .Add(
                            new FizzBuzzDto
                            {
                                TextColour = divisibilityChecker.FizzBuzzModel.TextColour,
                                Description = divisibilityChecker.FizzBuzzModel.Id == -1 ? sequence.ToString() : fizzBuzzDescriptionList[divisibilityChecker.FizzBuzzModel.Id - 1]
                            });
            }

            return new FizzBuzzResponse
            {
                FizzBuzzList = finalFizzBuzzOutputList
            };
        }
    }
}
