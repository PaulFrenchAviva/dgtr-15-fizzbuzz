﻿using System;
namespace Business
{
   public interface IFizzBuzzDescriptionListConfigurator
    {
       /// <summary>
       /// Gets fizz buzz description list based on some condition.
       /// </summary>
       /// <returns></returns>
       string[] GetFizzBuzzDescriptionList(DayOfWeek dayOfWeek);
    }
}
