﻿using System;

namespace Business
{
    /// <summary>
    /// Gets fizz buzz description list based on some condition.
    /// </summary>
    /// <returns></returns>
    public class FizzBuzzDescriptionListConfigurator : IFizzBuzzDescriptionListConfigurator
    {
        /// <summary>
        /// Gets fizz buzz description list based on some condition.
        /// </summary>
        /// <returns></returns>
        public string[] GetFizzBuzzDescriptionList(DayOfWeek dayOfWeek)
        {
            if (dayOfWeek == DayOfWeek.Wednesday)
            {
                return new string[] { "Wizz", "Wuzz", "Wizz Wuzz" };                
            }

            return new string[] { "Fizz", "Buzz", "Fizz Buzz" };
        }
    }
}