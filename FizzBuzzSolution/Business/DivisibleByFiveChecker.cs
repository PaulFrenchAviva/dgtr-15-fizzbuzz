﻿namespace Business
{
    public class DivisibleByFiveChecker : IDivisibilityChecker
    {
         public DTO.FizzBuzzDto FizzBuzzModel { get; private set; }
         public int Sequence { get; set; }

         public DivisibleByFiveChecker()
        {
            this.Sequence = 3;
            this.FizzBuzzModel = new DTO.FizzBuzzDto { Id = 2, TextColour = "green" };
        }

        public bool IsDivisible(int number)
        {
            return number % 5 == 0;
        }
    }
}