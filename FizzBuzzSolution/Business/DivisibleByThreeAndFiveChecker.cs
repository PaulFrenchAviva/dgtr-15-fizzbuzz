﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Business
{
    public class DivisibleByThreeAndFiveChecker : IDivisibilityChecker
    {
         public DTO.FizzBuzzDto FizzBuzzModel { get; private set; }
         public int Sequence { get; set; }

         public DivisibleByThreeAndFiveChecker()
        {
            this.Sequence = 1;
            this.FizzBuzzModel = new DTO.FizzBuzzDto { Id = 3, TextColour = "blue green" };
        }

        public bool IsDivisible(int number)
        {
            return number % 3 == 0 && number % 5 == 0;
        }
    }
}