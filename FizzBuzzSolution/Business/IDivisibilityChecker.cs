﻿using DTO;

namespace Business
{
    public interface IDivisibilityChecker
    {
        FizzBuzzDto FizzBuzzModel { get; }
        int Sequence { get; set; }
        bool IsDivisible(int number);
    }
}
