﻿namespace Business
{
    public class DefaultDivisibleChecker : IDivisibilityChecker
    {
         public DTO.FizzBuzzDto FizzBuzzModel { get; private set; }
         public int Sequence { get; set; }

         public DefaultDivisibleChecker()
        {
            this.Sequence = 999;
            this.FizzBuzzModel = new DTO.FizzBuzzDto { Id = -1, TextColour = "" };
        }

        public bool IsDivisible(int number)
        {
            return true;
        }
    }
}