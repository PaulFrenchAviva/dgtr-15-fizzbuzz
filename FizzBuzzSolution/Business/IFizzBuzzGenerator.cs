﻿using DTO;
namespace Business
{
    /// <summary>
    /// Contract for generating fizz buzz list.
    /// </summary>
    public interface IFizzBuzzGenerator
    {
        FizzBuzzResponse Generate(FizzBuzzRequest input);
    }
}
