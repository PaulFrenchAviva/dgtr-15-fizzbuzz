﻿namespace Business
{
    public class DivisibleByThreeChecker : IDivisibilityChecker
    {
        public DTO.FizzBuzzDto FizzBuzzModel { get; private set; }
        public int Sequence { get; set; }

        public DivisibleByThreeChecker()
        {
            this.Sequence = 2;
            this.FizzBuzzModel = new DTO.FizzBuzzDto { Id = 1, TextColour = "blue" };
        }

        public bool IsDivisible(int number)
        {
            return number % 3 == 0;
        }
    }
}