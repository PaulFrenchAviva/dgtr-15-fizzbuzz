﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;

namespace Business.Tests
{
    [TestFixture]
    public class DivisibleByThreeAndFiveCheckerTest
    {
        [TestCase(15, true)]
        public void WhenGivenANumberMultipleOfThreeAndFiveThenItShouldReturnTrue(int number, bool expected)
        {
            // Arrange.
            var divisibleByThreeAndFiveChecker = new DivisibleByThreeAndFiveChecker();

            // Act.
            bool actual = divisibleByThreeAndFiveChecker.IsDivisible(number);

            // Assert.
            NUnit.Framework.Assert.AreEqual(expected, actual);
            NUnit.Framework.Assert.AreEqual(3, divisibleByThreeAndFiveChecker.FizzBuzzModel.Id);
        }

        [TestCase(19, false)]
        public void WhenGivenANumberNotMultipleOfThreeAndFiveThenItShouldReturnFalse(int number, bool expected)
        {
            // Arrange.
            var divisibleByThreeAndFiveChecker = new DivisibleByThreeAndFiveChecker();

            // Act.
            bool actual = divisibleByThreeAndFiveChecker.IsDivisible(number);

            // Assert.
            NUnit.Framework.Assert.AreEqual(expected, actual);
        }
    }
}
