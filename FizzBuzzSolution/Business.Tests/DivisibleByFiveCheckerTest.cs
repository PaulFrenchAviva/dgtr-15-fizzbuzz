﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;

namespace Business.Tests
{
    [TestFixture]
    public class DivisibleByFiveCheckerTest
    {
        [TestCase(90,true)]
        public void WhenGivenANumberMultipleOfFiveThenItShouldReturnTrue(int number,bool expected)
        {
            // Arrange.
            var divisibleByFiveChecker = new DivisibleByFiveChecker();

            // Act.
            bool actual = divisibleByFiveChecker.IsDivisible(number);

            // Assert.
            NUnit.Framework.Assert.AreEqual(expected, actual);
        }

        [TestCase(19,false)]
        public void WhenGivenANumberNotMultipleOfThreeThenItShouldReturnFalse(int number, bool expected)
        {
            // Arrange.
            var divisibleByThreeChecker = new DivisibleByThreeChecker();

            // Act.
            bool actual = divisibleByThreeChecker.IsDivisible(number);

            // Assert.
            NUnit.Framework.Assert.AreEqual(expected, actual);
        }
    }
}
