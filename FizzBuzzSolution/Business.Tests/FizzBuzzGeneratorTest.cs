﻿using DTO;
using NUnit.Framework;
using Rhino.Mocks;
using System;
using System.Collections.Generic;

namespace Business.Tests
{
    [TestFixture]
    public class FizzBuzzGeneratorTest
    {
        private IList<IDivisibilityChecker> divisibilityCheckers;
        private IDivisibilityChecker divisibleByThreeChecker;
        private IDivisibilityChecker divisibleByFiveChecker;
        private IDivisibilityChecker divisibleByThreeAndFiveChecker;
        private IDivisibilityChecker defaultDivisibleChecker;
        private FizzBuzzGenerator fizzBuzzGenerator;
        private IFizzBuzzDescriptionListConfigurator fizzBuzzDescriptionListConfigurator;

        [SetUp]
        public void Initialize()
        {
            this.divisibleByThreeChecker = MockRepository.GenerateMock<IDivisibilityChecker>();
            this.divisibleByFiveChecker = MockRepository.GenerateMock<IDivisibilityChecker>();
            this.divisibleByThreeAndFiveChecker = MockRepository.GenerateMock<IDivisibilityChecker>();
            this.defaultDivisibleChecker = MockRepository.GenerateMock<IDivisibilityChecker>();
            this.fizzBuzzDescriptionListConfigurator = MockRepository.GenerateMock<IFizzBuzzDescriptionListConfigurator>();

            this.divisibilityCheckers = new List<IDivisibilityChecker>{
                divisibleByThreeChecker,divisibleByFiveChecker,divisibleByThreeAndFiveChecker,defaultDivisibleChecker
            };
            this.fizzBuzzGenerator = new FizzBuzzGenerator(divisibilityCheckers, fizzBuzzDescriptionListConfigurator);
        }

        [TestCase(-6)]
        public void WhenGivenNegativeNumberThenItShouldReturnErrorMessage(int number)
        {
            // Arrange.
            var input = new FizzBuzzRequest { Number = number };
            string expectedErrorMessage = "Enter a number between 1 and 1000.";           

            // Act.
            System.Diagnostics.Debugger.Launch();
            var response = fizzBuzzGenerator.Generate(input);

            // Assert.
            NUnit.Framework.Assert.AreEqual(expectedErrorMessage, response.ErrorMessage);
        }

        [TestCase(3)]
        public void WhenGivenValidNumberThenItShouldReturnFizzBuzzList(int number)
        {
            // Arrange.
            var input = new FizzBuzzRequest { Number = number };
            FizzBuzzResponse exptectedList = new FizzBuzzResponse
            {
                FizzBuzzList = new List<FizzBuzzDto> 
                                                                  { 
                                                                       new FizzBuzzDto { Description = "1" },
                                                                       new FizzBuzzDto { Description = "2" }, 
                                                                       new FizzBuzzDto { Description = "Fizz" } 
                                                                  }
            };

            var fizzBuzzDescriptionList = new string[] { "Fizz", "Fuzz", "Fizz Fuzz" };

            this.fizzBuzzDescriptionListConfigurator.Stub(p => p.GetFizzBuzzDescriptionList(DayOfWeek.Monday)).IgnoreArguments().Return(fizzBuzzDescriptionList);

            MockTheRequiredDependencies();

            // Act.
            var response = fizzBuzzGenerator.Generate(input);

            // Assert.
            NUnit.Framework.Assert.AreEqual(exptectedList.FizzBuzzList.Count, response.FizzBuzzList.Count);
        }

        [TestCase(0)]
        public void WhenGivenNumberOutOfLowerRangeThenItShouldReturnErrorMessage(int number)
        {
            // Arrange.
            var input = new FizzBuzzRequest { Number = number };
            string expectedErrorMessage = "Enter a number between 1 and 1000.";           

            // Act.
            var response = fizzBuzzGenerator.Generate(input);

            // Assert.
            NUnit.Framework.Assert.AreEqual(expectedErrorMessage, response.ErrorMessage);
        }

        [TestCase(1200)]
        public void WhenGivenNumberOutOfUpperRangeThenItShouldReturnErrorMessage(int number)
        {
            // Arrange.
            var input = new FizzBuzzRequest { Number = number };
            string expectedErrorMessage = "Enter a number between 1 and 1000.";

            // Act.
            var response = fizzBuzzGenerator.Generate(input);

            // Assert.
            NUnit.Framework.Assert.AreEqual(expectedErrorMessage, response.ErrorMessage);
        }


        [TestCase(3)]
        public void WhenDayOfTheWeekIsWednesdayThenTheFizzBuzzListShouldContainWizzWuzz(int number)
        {
            // Arrange.
            var input = new FizzBuzzRequest { Number = number };
            FizzBuzzResponse exptectedList = new FizzBuzzResponse
            {
                FizzBuzzList = new List<FizzBuzzDto> 
                                                                  { 
                                                                       new FizzBuzzDto { TextColour = "", Description = "1" },
                                                                       new FizzBuzzDto { TextColour = "", Description = "2" }, 
                                                                       new FizzBuzzDto { TextColour = "blue", Description = "Wizz" } 
                                                                  }
            };
            var fizzBuzzDescriptionList = new string[] { "Wizz", "Wuzz", "Wizz Wuzz" };

            // Mock dependencies.
            this.fizzBuzzDescriptionListConfigurator.Stub(p => p.GetFizzBuzzDescriptionList(DayOfWeek.Wednesday)).IgnoreArguments().Return(fizzBuzzDescriptionList);

            MockTheRequiredDependencies();

            // Act.
            // System.Diagnostics.Debugger.Launch();
            var response = fizzBuzzGenerator.Generate(input);

            // Assert.
            NUnit.Framework.Assert.AreEqual(exptectedList.FizzBuzzList[2].Description, response.FizzBuzzList[2].Description);
        }

        private void MockTheRequiredDependencies()
        {
            // Mock dependencies.            

            this.divisibleByThreeAndFiveChecker.Stub(p => p.Sequence).Return(1);
            this.divisibleByThreeChecker.Stub(p => p.Sequence).Return(2);
            this.divisibleByFiveChecker.Stub(p => p.Sequence).Return(3);
            this.defaultDivisibleChecker.Stub(p => p.Sequence).Return(4);

            // Mock other divisibility checkers for number 1  except default divisiblity checker.
            this.divisibleByThreeAndFiveChecker.Stub(p => p.IsDivisible(1)).Return(false);
            this.divisibleByThreeChecker.Stub(p => p.IsDivisible(1)).Return(false);
            this.divisibleByFiveChecker.Stub(p => p.IsDivisible(1)).Return(false);
            this.defaultDivisibleChecker.Stub(p => p.IsDivisible(1)).Return(true);
            this.defaultDivisibleChecker.Stub(p => p.FizzBuzzModel).Return(new FizzBuzzDto { Id = -1 });

            // Mock other divisibility checkers for number 2  except default divisiblity checker.
            this.divisibleByThreeAndFiveChecker.Stub(p => p.IsDivisible(2)).Return(false);
            this.divisibleByThreeChecker.Stub(p => p.IsDivisible(2)).Return(false);
            this.divisibleByFiveChecker.Stub(p => p.IsDivisible(2)).Return(false);
            this.defaultDivisibleChecker.Stub(p => p.IsDivisible(2)).Return(true);
            this.defaultDivisibleChecker.Stub(p => p.FizzBuzzModel).Return(new FizzBuzzDto { Id = -1 });

            // Mock other divisibleByThreeChecker checkers for number 3  to handler number 3.
            this.divisibleByThreeAndFiveChecker.Stub(p => p.IsDivisible(3)).Return(false);
            this.divisibleByThreeChecker.Stub(p => p.IsDivisible(3)).Return(true);
            this.divisibleByThreeChecker.Stub(p => p.FizzBuzzModel).Return(new FizzBuzzDto { Id = 1 });
        }
    }
}
