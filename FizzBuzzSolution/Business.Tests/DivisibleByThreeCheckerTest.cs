﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;

namespace Business.Tests
{
    [TestFixture]
    public class DivisibleByThreeCheckerTest
    {
        [TestCase(9,true)]
        public void WhenGivenANumberMultipleOfThreeThenItShouldReturnTrue(int number, bool expected)
        {
            // Arrange.
            var divisibleByThreeChecker = new DivisibleByThreeChecker();

            // Act.
            bool actual = divisibleByThreeChecker.IsDivisible(number);

            // Assert.
            NUnit.Framework.Assert.AreEqual(expected, actual);
            NUnit.Framework.Assert.AreEqual(1, divisibleByThreeChecker.FizzBuzzModel.Id);
        }

        [TestCase(19,false)]
        public void WhenGivenANumberNotMultipleOfThreeThenItShouldReturnFalse(int number, bool expected)
        {
            // Arrange.
            var divisibleByThreeChecker = new DivisibleByThreeChecker();

            // Act.
            bool actual = divisibleByThreeChecker.IsDivisible(number);

            // Assert.
            NUnit.Framework.Assert.AreEqual(expected, actual);
        }
    }
}
