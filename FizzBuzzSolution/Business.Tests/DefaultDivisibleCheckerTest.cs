﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;

namespace Business.Tests
{
    [TestFixture]
    public class DefaultDivisibleCheckerTest
    {
        [TestCase(2, true)]
        public void WhenGivenANumberWhichIsNotDivisibleByThreeOrFiveThenItShouldReturnTrue(int number, bool expected)
        {
            // Arrange.
            var defaultDivisibleChecker = new DefaultDivisibleChecker();

            // Act.
            bool actual = defaultDivisibleChecker.IsDivisible(number);

            // Assert.
            NUnit.Framework.Assert.AreEqual(expected, actual);
        }
    }
}
