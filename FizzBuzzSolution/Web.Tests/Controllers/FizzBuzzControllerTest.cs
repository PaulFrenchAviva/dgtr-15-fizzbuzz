﻿using Business;
using DTO;
using NUnit.Framework;
using Rhino.Mocks;
using System.Collections.Generic;
using System.Web.Mvc;
using Web.Controllers;
using System.Linq;
using Web.Models;
using System.Diagnostics;

namespace Web.Tests.Controllers
{
    [TestFixture]
    public class FizzBuzzControllerTest
    {
        private FizzBuzzController fizzBuzzController;
        private IFizzBuzzGenerator fizzBuzzGenerator;

        [SetUp]
        public void Initialize()
        {
            fizzBuzzGenerator = MockRepository.GenerateMock<IFizzBuzzGenerator>();

            fizzBuzzController = TestConfigure
                .ConfigureController<FizzBuzzController>(new FizzBuzzController(fizzBuzzGenerator));
        }

        [TestCase]
        public void WhenRequestedForIndexActionMethodThenItShouldReturnIndexView()
        {
            // Act.
            ViewResult result = fizzBuzzController.Index() as ViewResult;

            var inputModel = result.Model as FizzBuzzModel;

            // Assert.
            NUnit.Framework.Assert.AreEqual(0, inputModel.Number);
        }

        [TestCase(3)]
        public void WhenGivenARequestWithInputNumberThenItShouldReturnViewWithFizzBuzzList(int number)
        {
            // Arrange.
            var input = new FizzBuzzModel { Number = number };
            var fizzBuzzList = new List<FizzBuzzDto> 
            { 
                new FizzBuzzDto{ Description = "1" },
                new FizzBuzzDto{ Description = "2" },
                new FizzBuzzDto{ Description = "Fizz"}
            };

            MockRequiredDependencies(input, fizzBuzzList);

            // Act.
            var result = Act(input);

            // Assert.
            NUnit.Framework.Assert.AreEqual(3, result.FizzBuzzList.Count);
        }     

       
        [TestCase(-9)]
        [TestCase(0)]
        [TestCase(1200)]
        public void TestNegativeNumberAndNumberOutOfRangeScenarios(int number)
        {
            // Arrange.
            var input = new FizzBuzzModel { Number = number };
            IList<FizzBuzzDto> fizzBuzzList = new List<FizzBuzzDto>();

            MockRequiredDependencies(input, fizzBuzzList);

            // Act.
            Debugger.Launch();
            var result = Act(input);

            // Assert.
            NUnit.Framework.Assert.IsEmpty(result.FizzBuzzList);
        }

        [TestCase(3)]
        public void WhenDayOfWeekIsWednesdayThenFizzBuzzListShouldContainWordsWizzWuzz(int number)
        {
            // Arrange.
            var input = new FizzBuzzModel { Number = number };
            var fizzBuzzList = new List<FizzBuzzDto> 
            { 
                new FizzBuzzDto{ Description = "1" , TextColour = ""},
                new FizzBuzzDto{ Description = "2" , TextColour = ""},
                new FizzBuzzDto{ Description = "Wizz" , TextColour = "blue"}
            };

            MockRequiredDependencies(input, fizzBuzzList);

            // Act.
            var result = Act(input);

            // Assert.
            NUnit.Framework.Assert.AreEqual(fizzBuzzList[2].Description, result.FizzBuzzList.LastOrDefault().Description);
        }

        private void MockRequiredDependencies(FizzBuzzModel input, IList<FizzBuzzDto> outputList)
        {
            fizzBuzzGenerator.Stub(p => p.Generate(null)).IgnoreArguments().Return(new FizzBuzzResponse { FizzBuzzList = outputList });
        }

        private FizzBuzzModel Act(FizzBuzzModel input)
        {
            ViewResult result = fizzBuzzController.GetFizzBuzzList(input) as ViewResult;

            return result.Model as FizzBuzzModel;
        }
    }
}
