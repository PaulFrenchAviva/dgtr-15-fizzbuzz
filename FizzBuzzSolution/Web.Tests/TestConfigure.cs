﻿using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Web.Tests
{
    public static class TestConfigure
    {
        /// <summary>
        /// Configures the controller for test.
        /// </summary>
        /// <param name="controller"> The controller. </param>
        /// <param name="name"> The name of the controller. </param>
        /// <param name="method"> The Http method.Default is Get. </param>
        /// <returns>The API controller. </returns>
        public static T ConfigureController<T>(Controller controller) where T : class
        {
            controller.ControllerContext = new ControllerContext()
            {
                Controller = controller,
                RequestContext = new RequestContext(new MockHttpContext(), new RouteData())
            };
            return controller as T;
        }

        private class MockHttpContext : HttpContextBase
        {
            private readonly IPrincipal _user = new GenericPrincipal(
                     new GenericIdentity("someUser"), null /* roles */);

            public override IPrincipal User
            {
                get
                {
                    return _user;
                }
                set
                {
                    base.User = value;
                }
            }
        }
    }
}

